int max2(int a, int b){
    // Action: Retourne le plus grand nombre parmis les 2
    if (a >= b){
        return a;
    }
    return b;
}

int max3(int a, int b, int c){
    // Action: Retourne le plus grand nombre parmis les 3 en utilisant max2()
    return max2(max2(a,b),c);
}

void testMax(){
    Ut.afficher("Entrez un premier nombre: ");
    int nb1 = Ut.saisirEntier();
    Ut.afficher("Entrez un second nombre: ");
    int nb2 = Ut.saisirEntier();
    Ut.afficherSL("Le nombre le plus grand est " + max2(nb1,nb2));
    Ut.afficher("Entrez un premier nombre: ");
    nb1 = Ut.saisirEntier();
    Ut.afficher("Entrez un deuxième nombre: ");
    nb2 = Ut.saisirEntier();
    Ut.afficher("Entrez un troisième nombre: ");
    int nb3 = Ut.saisirEntier();
    Ut.afficher("Le nombre le plus grand est " + max3(nb1,nb2,nb3));
}

void repeteCarac(int nb, char car){
    // Action: Affiche nb caractère 'car' à partir de la position
    for(int i = 1; i < nb; i++){
        Ut.afficher(car);
    }
}

void pyramideSimple(int h, char c){
    // Action: Affiche une pyramide de heuteur h constitué de lignes répétant le caractère c
    for (int i = 1; i <= h; i++){
        for (int j = h; j > i; j--){
            Ut.afficher(" ");
        }
        repeteCarac(i*2,c);
        Ut.afficherSL("");
    }
}

void testPyramideSimple(){
    // Action: Demande à l'utilisateur une hauteur de pyramide et un caractère, puis affiche une pyramide
    Ut.afficher("Entrez la hauteur de la pyramide: ");
    int hauteur = Ut.saisirEntier();
    Ut.afficher("Entrez un caractère qui sera utilisé dans la pyramide: ");
    char car = Ut.saisirCaractere();
    pyramideSimple(hauteur, car);
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissante(int nb1, int nb2){
    // Action: Affiche les nombres croissants entre les deux nombres
    for (int i = 0; i <= nb2-nb1; i++){
        Ut.afficher((nb1+i)%10 + " ");
    }
}

void afficheNombresDecroissante(int nb1, int nb2){
    // Action: Affiche les nombres décroissants entre les deux nombres
    for (int i = 0; i <= nb2-nb1; i++){
        Ut.afficher((nb2-i)%10 + " ");
    }
}

void pyramideElaboree(int h){
    // Action: Créer la pyramide de chiffres
    for (int i = 1; i <= h; i++){
        for (int j = h; j+h -i > i; j--){
            Ut.afficher("*");
        }
        afficheNombresCroissante(i,(2*i)-2);
        Ut.afficher(((2*i)-1)%10);
        Ut.afficher(" ");
        afficheNombresDecroissante(i,(2*i)-2);
        Ut.afficherSL("");
    }
}

int nbCiffres(int n){
    // Action: retourne le nombre de chiffre de n
    int nbChiffres = 0;
    while (n > 0){
        n = n / 10;
        nbChiffres ++;
    }
    return nbChiffres;
}

int nbChiffresDuCarre(int n){
    // Action: retourne le nombre de chiffre de n au carré
    return nbCiffres(n*n);
}

void amiEntreNombres(int q, int p){
    // Action: Verif si p et q sont amis
    int score1 = 0;
    int score2 = 0;
    for (int i = 1; i < q; i++){
        if (q % i == 0){
            score1 += i;
        }
    }
    for (int i = 1; i < p; i++){
        if (p % i == 0){
            score2 += i;
        }
    }
    if ((score1 == p) || (score2 == q)) {
        Ut.afficherSL("Les nombres " + q + " et "+ p + " sont amis");
    }
}

void nbrAmis(){
    // Action: Aff tout les amis qui sont inférieur ou égale à q et p
    int q = 0;
    int p = 0;
    while (q < 1){
        Ut.afficher("Veuillez entrer un nombre (qui doit être supérieur ou égale à 1): ");
        q = Ut.saisirEntier();
    }
    while (p < 1){
        Ut.afficher("Veuillez entrer un deuxième nombre (qui doit être supérieur ou égale à 1): ");
        p = Ut.saisirEntier();
    }
    for (int i = 1; i <= q; i++){
        for (int j = 1; j <= p; j++){
            amiEntreNombres(i,j);
        }
    }
    Ut.afficher("Fin...");
}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c){
    // Action: Renvoie la racine carré de c
    int racine = (int) Math.sqrt(c);
    return ( c == racine*racine ? racine : -1);
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    // Action : Vérif si c'est un carrer parfait
    return ((racineParfaite(nb) != -1) ? true : false);
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){
    // Action : Vérif si c'est un tirangle rectangle
    int hypotenuseCarre = (int)Math.pow(c1,2) + (int)Math.pow(c2,2);
    return (estCarreParfait(hypotenuseCarre) ? true : false);
}

int nbrTriangleRectangle(int n){
    // Action : Calcul le nombre de triangle rectangles
    int score = 0;
    for (int i = 1; i < n; i++){
        for (int j = 1; j < n; j++){
            for (int k = 1; k < n; k++){
                if (i+j+k <= n){
                    score ++;
                }
            }
        }
    }
    return (score);
}

boolean estSyracusien(int n){
    // Action: Verif si un nombre est Syracusien
    if (n % 2 == 0){
        n = n / 2;
    } else {
        n = (n * 3) + 1;
    }
    return ((n == 1)? true : false);
}

boolean estSyracusienAvecMaxOp(int n, int maxOps){
    // Action: Vérif si le nombre n est syracusien avec un nombre maximum d'essais
    for (int i = 1; i <= maxOps ; i++) {
        if (n == 1) {
            return true;
        } else if (n % 2 == 0){
            n = n / 2;
        } else {
            n = (n*3) +1;
        }
    }
    return estSyracusien(n);
}

int valApprocheExp(int n){
    // Action : affiche la valeur exposé approché du nombre N
    //int pow = (int) Math.pow(n, n);
    //Ut.afficher(pow);
    int temp = (int) 0;
    for (int i = 1; i <= n; i++){
        temp +=  i + 1 / n;

    }
        return temp;
}

////////////////////
// TP SUITE ORDRE //
////////////////////
void suiteOrdre(int n) {
    // Action :: Affiche pour chaque entier i de 2 à n, les valeurs i, vi, p(vi)

    // Var
    int u1 = 1;
    int u2 = 1;
    int u3 = 1;
    float vi = 1;

    // AC :: Vérifie si N est bien supérieur a 2 et redemande a l'utilisateur une nouvelle valeur si inférieur a 2
    while (n < 2){
        Ut.afficher("La valeur doit être supérieur à 2");
        n = Ut.saisirEntier();
    }
    // AC :: Fibo
    for (int i = 2; i <= n; i++){
        u3 = u2 - u1 ;
        vi = (float) u3 / u2;
        u1 = u2;
        u2 = u3;
        // AC :: Aff résults
        Ut.afficherSL("i= "+ i +" vi= "+ vi);
    }
}

void suiteOrEpsilon(int n){
    // AC :: Vérifie si N est bien supérieur a 2 et redemande a l'utilisateur une nouvelle valeur si inférieur a 2
    while (n < 2){
        Ut.afficher("La valeur doit être supérieur à 2");
        n = Ut.saisirEntier();
    }

        }
void menuSuite(){
    // Action :: Affiche un menu pour gerer la fonction fibonacci
    Ut.afficherSL("MENU SUITE");
    Ut.afficherSL("Choisir fonction:");
    Ut.afficherSL("1. suiteOrOrdre");
    Ut.afficherSL("2. suiteOrEplsilon");
    Ut.afficherSL("3. Quitter");
    // AC :: On demande de saisir un entier entre 1 et 3 pour choisir une fonction
    int fnc = Ut.saisirEntier();
    while (fnc >= 4 || fnc <= 0){
        Ut.afficherSL("Cette fonction n'existe pas");
        fnc = Ut.saisirEntier();
    }
    // AC :: si 1 est chosi alors on lance la fonction suiteOrdre
    if (fnc == 1){
        suiteOrdre(0);
    }
    // AC :: Si 2 est choisi alors on lance la fonction suiteOrEpsilon
    if (fnc == 2){
        suiteOrEpsilon(0);
    }
    // AC :: Si 3 est choisi alors on quitte la foncion.
    if (fnc == 3){
        return;
    }



}

///////////////////
// TP MARIN IVRE //
///////////////////
int nbAleatoire(){
    // Action :: Donne un nombre aléatoire entre 1 et 100
    int random = Ut.randomMinMax(1, 100);

    // Attribution selon les probabilités demandées
    if (random <= 50) {
        // 50% chance : nombre entre 1 et 50
        return Ut.randomMinMax(1, 50);
    } else if (random <= 70) {
        // 20% chance : nombre entre 51 et 70
        return Ut.randomMinMax(51, 70);
    } else if (random <= 90) {
        // 20% chance : nombre entre 71 et 90
        return Ut.randomMinMax(71, 90);
    } else {
        // 10% chance : nombre entre 91 et 100
        return Ut.randomMinMax(91, 100);
    }
}

void arivobato(){

}

void afficheplance(){

}

void main (){
    menuSuite();
}
